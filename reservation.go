package tms

import (
	"bytes"
	"context"
	"encoding/xml"
	"net/http"
	"time"

	"github.com/aws/aws-xray-sdk-go/xray"
)

type ConfirmReservationRequest struct {
	XMLName          xml.Name `xml:"ApiConfirmReservationRequest"`
	BookingReference string   `xml:"BookingReference"`
	Payment          Payment  `xml:"BookingPayment"`
}

type Payment struct {
	Value             float64  `xml:"Amount"`
	AuthorizationCode *string  `xml:"AuthNo,omitempty"`
	CardFee           *float64 `xml:"CardFee,omitempty"`
	CardType          string   `xml:"CardType"`
	LastCardDigits    string   `xml:"Last4Digits"`
	PassengerNumber   *uint64  `xml:"PassengerNumber,omitempty"`
	Reference         string   `xml:"PaymentRef"`
	SecurityKey       *string  `xml:"SecurityKey,omitempty"`
}

type ConfirmReservationResponse struct {
	XMLName xml.Name `xml:"ApiConfirmReservationCommandResponse"`
	Success bool     `xml:"Success"`
}

type CreateReservationRequest struct {
	XMLName xml.Name `xml:"ApiInitialiseReservationRequest"`

	Booking Booking `xml:"Booking"`
}

type CreateReservationResponse struct {
	XMLName xml.Name `xml:"ApiInitialiseReservationCommandResponse"`

	Booking struct {
		Reference      string   `xml:"BookingReference"`
		BalanceDueDate Zoneless `xml:"BalanceDueDate"`
	} `xml:"Booking"`
}

type Booking struct {
	//Flights    []BookingFlight    `xml:"Flights>BookingFlight,omitempty"`
	Passengers []BookingPassenger `xml:"Passengers>BookingPassenger"`
	Tours      []BookingTour      `xml:"Tours>BookingTour"`
}

type BookingTour struct {
	Code            string                `xml:"TourCode"`
	PassengerType   string                `xml:"TourPaxTypeCode"`
	StartDate       time.Time             `xml:"StartDate"`
	Duration        uint64                `xml:"Duration"`
	SpecialRequests string                `xml:"SpecialRequest"`
	Flights         []BookingTourFlight   `xml:"Flights>BookingTourFlight,omitempty"`
	Hotels          []BookingTourHotel    `xml:"Hotels>BookingTourHotel"`
	Grounds         []BookingTourGround   `xml:"Grounds>BookingTourGroundItem"`
	Passengers      []PassengerAssignment `xml:"Passengers>BookingPassengerIdentity"`
}

type BookingTourHotel struct {
	HotelCode     string        `xml:"HotelCode"`
	Rooms         []BookingRoom `xml:"Rooms>BookingRoom"`
	TourHotelCode string        `xml:"TourHotelCode,omitempty"`
}

type BookingRoom struct {
	Duration         uint64                `xml:"Duration"`
	Guests           []PassengerAssignment `xml:"Guests>BookingGuest"`
	PriceCode        string                `xml:"PriceCode,omitempty"`
	RoomTypeCode     string                `xml:"RoomTypeCode"`
	StartDate        time.Time             `xml:"StartDate"`
	TourRoomTypeCode string                `xml:"TourRoomTypeCode,omitempty"`
}

type BookingTourGround struct {
	Duration   uint64                `xml:"Duration"`
	Code       string                `xml:"GroundCode"`
	Passengers []PassengerAssignment `xml:"PassengerNumbers>BookingPassengerIdentity"`
	StartDate  time.Time             `xml:"StartDate"`
	TourCode   string                `xml:"TourGroundCode,omitempty"`
}

type BookingTourFlight struct {
	SKU        string                `xml:"FlightCode"`
	Legs       []FlightLeg           `xml:"FlightLegs>FlightLeg"`
	Seats      []BookingFlightSeat   `xml:"FlightSeats>BookingFlightSeat"`
	Passengers []PassengerAssignment `xml:"Passengers>BookingPassengerIdentity"`
	TourSKU    string                `xml:"TourFlightCode,omitempty"`
}

type BookingFlight struct {
	Flight
	Seats      []BookingFlightSeat   `xml:"FlightSeats>BookingFlightSeat"`
	Passengers []PassengerAssignment `xml:"Passengers>BookingPassengerIdentity"`
}

type BookingFlightSeat struct {
	Passengers []PassengerAssignment `xml:"Passengers>BookingPassengerIdentity"`
}

type PassengerAssignment struct {
	PassengerID uint64 `xml:"PassengerNumber"`
}

type BookingPassenger interface {
	Lead() bool
	ID() uint64
}

type ExistingCustomerPassenger struct {
	CustomerCode string `xml:"CustomerCode"`
	IsLead       bool   `xml:"IsLead"`
	PassengerID  uint64 `xml:"PassengerNumber"`
}

func (e ExistingCustomerPassenger) ID() uint64 {
	return e.PassengerID
}

func (e ExistingCustomerPassenger) Lead() bool {
	return e.IsLead
}

// CreateReservation creates a new reservation, temporarily holding the inventory for the reservation.
func (c *Client) CreateReservation(ctx context.Context, params CreateReservationRequest) (*CreateReservationResponse, error) {

	ctx, segment := xray.BeginSubsegment(ctx, "tms.Client.InitReservation")

	reqURL := c.BaseURL
	reqURL.Path += "/Reservations/InitialiseReservation"

	var buffer []byte
	b := bytes.NewBuffer(buffer)

	xc := xml.NewEncoder(b)

	xc.Indent("", "    ")

	if err := xc.Encode(params); err != nil {
		segment.Close(err)
		return nil, err
	}

	req, err := http.NewRequest(http.MethodPost, reqURL.String(), b)

	if err != nil {
		segment.Close(err)
		return nil, err
	}

	resp := &CreateReservationResponse{}

	if err := c.RequestXML(ctx, req, resp); err != nil {
		segment.Close(err)
		return nil, err
	}

	segment.Close(nil)
	return resp, nil
}

// ConfirmReservation will confirm a previously created reservation
func (c *Client) ConfirmReservation(ctx context.Context, params ConfirmReservationRequest) (*ConfirmReservationResponse, error) {

	ctx, segment := xray.BeginSubsegment(ctx, "tms.Client.ConfirmReservation")

	segment.AddMetadataToNamespace("tms", "booking_reference", params.BookingReference)

	reqURL := c.BaseURL
	reqURL.Path += "/Reservations/ConfirmReservation"

	var buffer []byte
	b := bytes.NewBuffer(buffer)

	xc := xml.NewEncoder(b)

	xc.Indent("", "    ")

	if err := xc.Encode(params); err != nil {
		segment.Close(err)
		return nil, err
	}

	req, err := http.NewRequest(http.MethodPost, reqURL.String(), b)

	if err != nil {
		segment.Close(err)
		return nil, err
	}

	resp := &ConfirmReservationResponse{}

	if err := c.RequestXML(ctx, req, resp); err != nil {
		segment.Close(err)
		return nil, err
	}

	segment.Close(nil)
	return resp, nil
}
