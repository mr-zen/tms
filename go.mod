module bitbucket.org/mr-zen/tms

go 1.12

require (
	github.com/DATA-DOG/go-sqlmock v1.3.3 // indirect
	github.com/aws/aws-sdk-go v1.19.27 // indirect
	github.com/aws/aws-xray-sdk-go v0.9.4
	github.com/briandowns/spinner v1.7.0
	github.com/cihub/seelog v0.0.0-20170130134532-f561c5e57575 // indirect
	github.com/pbnjay/harhar v0.0.0-20141101043836-ff0c3491c71b
	github.com/prometheus/client_golang v1.2.1
	github.com/seborama/govcr v0.0.0-20170609174344-a5922e8c053f
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/viper v1.3.2
	github.com/urfave/cli v1.20.0
	golang.org/x/crypto v0.0.0-20190617133340-57b3e21c3d56
	golang.org/x/net v0.0.0-20190613194153-d28f0bde5980
	golang.org/x/sys v0.0.0-20191105231009-c1f44814a5cd
)
