package tms

import (
	"bytes"
	"context"
	"encoding/xml"
	"net/http"
	"time"
)

// Direction represents a service offering direction.
type Direction string

const (
	// DirectionInbound is inbound
	DirectionInbound Direction = "Inbound"
	// DirectionOutbound is outbound
	DirectionOutbound Direction = "Outbound"
)

// AvailabilityResponse is the response given to an availability search
type AvailabilityResponse struct {
	xml.Name       `xml:"ApiGetAvailabilityQueryResponse"`
	Tours          []TourAvailability  `xml:"Tours>Tour"`
	Hotels         []HotelAvailability `xml:"Hotels>ResponseHotel"`
	Flights        []Flight            `xml:"Flights>Flight"`
	GroundServices []GroundService     `xml:"Grounds>GroundItem"`
}

// GroundService represents a single ground service optionally provided with a Tour.
type GroundService struct {
	Category    string                `xml:"CategoryName"`
	Description string                `xml:"Description"`
	Name        string                `xml:"Name"`
	Code        string                `xml:"GroundCode"`
	Options     []GroundServiceOption `xml:"GroundOptions>GroundOption"`
	Direction   Direction             `xml:"Direction"`
}

// GroundServiceOption represents a bookable option for a ground service
type GroundServiceOption struct {
	Duration uint64 `xml:"Duration"`
	Quantity uint64 `xml:"NumberOfUnits"`
	Prices   []*struct {
		Currency string  `xml:"CurrencyCode"`
		Price    float64 `xml:"TotalPrice"`
	} `xml:"Prices>PriceItem"`
	Date Zoneless `xml:"StartDate"`
}

// Flight represents a bookable flight package
type Flight struct {
	Duration int
	Notes    []FlightNote  `xml:"Errata>Erratum"`
	SKU      string        `xml:"FlightCode"`
	Legs     FlightRoute   `xml:"FlightLegs>FlightLeg"`
	Prices   []FlightPrice `xml:"Prices>PriceItem"`
}

// FlightPrice represents a pricing option.
type FlightPrice struct {
	TotalPrice  float64
	Description string
}

// FlightNote represents additional notes against a flight package.
type FlightNote struct {
	Description string
	Title       *string `xml:"Name"`
}

// TourAvailability represents data for a tour's availability.
type TourAvailability struct {
	TourCode    string      `xml:"TourCode"`
	Name        string      `xml:"TourName"`
	Features    []Feature   `xml:"Features>Feature"`
	Description string      `xml:"TourDescription"`
	Duration    uint        `xml:"TourDuration"`
	ImageTexts  []ImageText `xml:"ImageTexts>ImageText"`

	RegionName string `xml:"RegionName"`
	RegionCode string `xml:"RegionCode"`

	ResortCode string `xml:"ResortCode"`

	Options []TourOption `xml:"TourOptions>TourOption"`
}

// TourOption represents an option for booking a given tour.
type TourOption struct {
	Start                       Zoneless         `xml:"StartDate"`
	End                         Zoneless         `xml:"ReturnDate"`
	GroupSize                   int              `xml:"GroupSize"`
	CurrencyCode                string           `xml:"CurrencyCode"`
	Price                       float64          `xml:"SellingPrice"`
	AvailableSpaces             uint             `xml:"Available"`
	GradeCode                   string           `xml:"GradeCode"`
	GradeDescription            string           `xml:"GradeDescription"`
	Items                       []TourOptionItem `xml:"TourItems>TourItem"`
	Prices                      []TourPriceItem  `xml:"Prices>PriceItem"`
	AlternativeFlightsAvailable bool             `xml:"AllowGdsFlights"`
}

// TourOptionItem represents a single bookable option within a tour
type TourOptionItem struct {
	Order       int      `xml:"OrderNo"`
	Date        Zoneless `xml:"Date"`
	Duration    uint     `xml:"Duration"`
	Description string   `xml:"Description"`
	Type        string   `xml:"Type"`
	Gender      Gender   `xml:"Gender"`
	Object      string   `xml:"ObjectCode"`
	Code        string   `xml:"ItemCode"`
}

// TourPriceItem represents a single price element for a tour item
type TourPriceItem struct {
	CurrencyCode string  `xml:"CurrencyCode"`
	Description  string  `xml:"Description"`
	PriceCode    string  `xml:"PriceCode"`
	Total        float64 `xml:"TotalPrice"`
}

// HotelAvailability represents availability data for a hotel
type HotelAvailability struct {
	Code        string                  `xml:"HotelCode"`
	CountryCode string                  `xml:"CountryCode"`
	CountryID   string                  `xml:"CountryId"`
	RegionCode  string                  `xml:"RegionCode"`
	Name        string                  `xml:"HotelName"`
	ResortCode  string                  `xml:"ResortCode"`
	Rooms       []HotelRoomAvailability `xml:"Rooms>Room"`
}

// HotelRoomAvailability represents the availability for a hotel’s rooms
type HotelRoomAvailability struct {
	RoomTypes []HotelRoomType `xml:"RoomTypes>RoomType"`
}

// HotelRoomType represents the details a single hotel room type
type HotelRoomType struct {
	AvailabilityStatus string
	BoardName          string
	RoomName           string
	StartDate          Zoneless `xml:"Date"`
	Duration           int      `xml:"Duration"`
	RoomTypeCode       string
	Prices             []HotelRoomPrice `xml:"Prices>PriceItem"`
}

// HotelRoomPrice represents a single price for a hotel room
type HotelRoomPrice struct {
	CurrencyCode string
	Description  string
	TotalPrice   float64
	PriceCode    string
}

// HotelPrice represents a single price for a hotel stay
type HotelPrice struct {
	Currency    string  `xml:"CurrencyCode"`
	Description string  `xml:"Description"`
	Price       float64 `xml:"TotalPrice"`
	Code        string  `xml:"PriceCode"`
}

// Feature represents a single feature of another datum
type Feature struct {
	Name               string `xml:"FeatureName"`
	CategoryName       string `xml:"FeatureValue>CategoryName"`
	CharacteristicName string `xml:"FeatureValue>CharacteristicName"`
	Value              string `xml:"FeatureValue>Value"`
	Flags              int    `xml:"FeatureValue>Flags"`
}

// FITAvailabilityRequest represents a full request to check service availability
type FITAvailabilityRequest struct {
	XMLName xml.Name `xml:"ApiGetAvailabilityRequest"`
	//Region  string   `xml:"RegionCode"`
	Country string `xml:"CountryCode"`

	IncludeFlights bool `xml:"IncludeFlights"`

	Flight *FlightAvailabilityRequest `xml:"Flight,omitempty"`

	IncludeHotels bool                      `xml:"IncludeHotels"`
	Hotel         *HotelAvailabilityRequest `xml:"Hotel,omitempty"`

	IncludeGrounds bool                              `xml:"IncludeGrounds"`
	Ground         *GroundServiceAvailabilityRequest `xml:"Ground,omitempty"`
	IncludeTours   bool                              `xml:"IncludeTours"`
	Tour           *TourAvailabilityRequest          `xml:"Tour,omitempty"`
}

// TourAvailabilityRequest represents a request for tour availability
type TourAvailabilityRequest struct {
	Start         time.Time `xml:"StartDate"`
	End           time.Time `xml:"EndDate"`
	Duration      int       `xml:"Duration"`
	Code          string    `xml:"TourCode"`
	PassengerType string    `xml:"TourPaxTypeCode"`
	Rooms         []Rooming `xml:"Rooms>Room"`
}

// GroundServiceAvailabilityRequest represents a request for ground services
type GroundServiceAvailabilityRequest struct {
	EndDate    time.Time   `xml:"EndDate"`
	Passengers []RoomGuest `xml:"Participants"`
	StartDate  time.Time   `xml:"StartDate"`
	CategoryID uint64      `xml:"GroundCategoryId"`
}

// HotelAvailabilityRequest represents a request for availability for a
// particular hotel or room
type HotelAvailabilityRequest struct {
	ResortCode string
	HotelCode  string
	StartDate  time.Time
	Nights     uint      `xml:"Duration"`
	Rooms      []Rooming `xml:"Rooms>Room"`
}

// Rooming maps hotel guests to a hotel room
type Rooming struct {
	Guests      []RoomGuest          `xml:"Guests>Guest"`
	ID          uint                 `xml:"RoomId"`
	Replacement *TourRoomReplacement `xml:"TourRoomDetails,omitempty"`
}

type TourRoomReplacement struct {
	Code     string   `xml:"TourCode"`
	Date     Zoneless `xml:"TourDate"`
	Order    int      `xml:"OrderNo"`
	ItemDate Zoneless `xml:"ItemDate"`
}

// RoomGuest represents the parameters of a hotel room guest that may affect
// availability and suitability
type RoomGuest struct {
	Age    uint
	Gender Gender
}

// FlightAvailabilityRequest represenst a request to search for available
// flights
type FlightAvailabilityRequest struct {
	Adults        uint `xml:"adults"`
	CabinClass    string
	DirectOnly    bool
	FareType      string
	Segments      []FlightSegment `xml:"FlightLegs>FlightLeg"`
	Seats         []FlightSeat    `xml:"FlightSeats>FlightSeat"`
	InfantsHeld   uint
	InfantsSeated uint
	StartDate     time.Time
	Airlines      []string `xml:"AirlineCodes>Airline,omitempty"`

	TourFlight *TourFlightReplacement `xml:"TourFlightDetails,omitempty"`
}

type TourFlightReplacement struct {
	Code     string   `xml:"TourCode"`
	Date     Zoneless `xml:"TourDate"`
	Order    int      `xml:"OrderNo"`
	ItemDate Zoneless `xml:"ItemDate"`
}

// FlightSegment represents a single segment of a flight route.
type FlightSegment struct {
	Date        time.Time `xml:"Date"`
	AirportFrom string    `xml:"FromAirport"`
	Index       uint      `xml:"Sequence"`
	AirportTo   string    `xml:"ToAirport"`
}

// FlightSeat represents a seat on the flight which will be booked by one or
// more passengers
type FlightSeat struct {
	Passengers []FlightPassenger `xml:"Passengers>Passenger"`
}

// FlightPassenger represents a single passenger within a flight
type FlightPassenger struct {
	Age         uint
	DateOfBirth time.Time
}

// ApiGetAvailabilityRequest represents all parameters available for performing
// availability searches.
type ApiGetAvailabilityRequest struct {
	IncludeTours bool      `xml:"IncludeTours"`
	StartDate    time.Time `xml:"Tour>StartDate"`
	EndDate      time.Time `xml:"Tour>EndDate"`
	TourCode     string    `xml:"Tour>TourCode"`
	TourPaxType  string    `xml:"Tour>TourPaxTypeCode"`
	Age          int       `xml:"Tour>Rooms>Room>Guests>Guest>Age"`
	Gender       Gender    `xml:"Tour>Rooms>Room>Guests>Guest>Gender"`
	RoomID       int       `xml:"Tour>Rooms>Room>RoomId"`
}

// Gender represents a customer/passenger Gender.
type Gender string

const (
	// GenderFemale is the female gender
	GenderFemale Gender = "F"

	// GenderOther is for when gender is neither M or F
	GenderOther Gender = "O"

	// GenderNone is for when gender is not applicable
	GenderNone Gender = "X"

	// GenderUnknown is for when gender is applicable but unknown
	GenderUnknown Gender = "U"

	// GenderMale is the make gender
	GenderMale Gender = "M"

	TourPaxTypeDefault string = "25-I-288"
)

// GetFITAvailability performs an availability search with the given parameters
func (c Client) GetFITAvailability(ctx context.Context, req FITAvailabilityRequest) (*AvailabilityResponse, error) {
	reqURL := c.BaseURL
	reqURL.Path += "/Availability/Search"

	content, err := xml.MarshalIndent(req, "", "\t")

	if err != nil {
		return nil, err
	}

	rq, err := http.NewRequest(http.MethodPost, reqURL.String(), bytes.NewBuffer(content))

	rq.Header.Set("Content-Type", "application/xml")

	if err != nil {
		return nil, err
	}

	rd := &AvailabilityResponse{}
	err = c.RequestXML(ctx, rq, rd)

	if err != nil {
		return nil, err
	}

	return rd, err
}

func (c Client) GetAvailability(ctx context.Context, tourCode string) ([]TourAvailability, error) {

	var av AvailabilityResponse

	data := ApiGetAvailabilityRequest{
		IncludeTours: true,
		TourCode:     tourCode,
		TourPaxType:  TourPaxTypeDefault,
		StartDate:    time.Now().Truncate(24 * time.Hour),
		EndDate:      time.Now().AddDate(10, 0, 0).Truncate(24 * time.Hour),
		Age:          18,
		RoomID:       1,
		Gender:       GenderUnknown,
	}

	reqURL := c.BaseURL
	reqURL.Path += "/Availability/Search"

	content, err := xml.MarshalIndent(data, "", "\t")

	if err != nil {
		return av.Tours, err
	}

	req, err := http.NewRequest(http.MethodPost, reqURL.String(), bytes.NewBuffer(content))

	if err != nil {
		return av.Tours, err
	}

	err = c.RequestXML(ctx, req, &av)

	return av.Tours, err

}
