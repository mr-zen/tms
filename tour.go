package tms

import (
	"context"
	"encoding/xml"
	"net/http"
	"time"
)

type Tour struct {
	XMLName xml.Name `xml:"Tour"`

	Name        string `xml:"TourName"`
	Code        string `xml:"TourCode"`
	Description string `xml:"TourDescription"`
	Duration    uint   `xml:"TourDuration"`
	Flags       uint32 `xml:"TourFlags"`

	CountryCode string `xml:"CountryCode"`
	CountryName string `xml:"CountryName"`

	ResortCode string `xml:"ResortCode"`
	ResortName string `xml:"ResortName"`

	Departures []Departure `xml:"Departures>TourDeparture"`
}

// Departure represents a single departure.
type Departure struct {
	XMLName   xml.Name `xml:"TourDeparture" json:"-"`
	Start     Zoneless `xml:"DepartureDate" json:"start_date"`
	End       Zoneless `xml:"ReturnDate" json:"end_date"`
	GroupSize uint     `json:"group_size"`
	GroupType string   `xml:"GroupTypeDescription" json:"group_type"`
	Grade     string   `json:"grade"`
	Price     float64  `xml:"SellingPrice" json:"selling_price"`
	Spaces    uint     `xml:"Available" json:"spaces"`

	StatusInfo      string `xml:"AvailabilityStatusInfo" json:"status_info"`
	Status          string `xml:"AvailabilityStatus" json:"status"`
	AllowGDSFlights bool   `xml:"AllowGdsFlights" json:"gds_flights"`
}

type apiToursAndDeparturesQueryResponse struct {
	XMLName xml.Name `xml:"ApiToursAndDeparturesQueryResponse"`
	Tours   []Tour   `xml:"Tours>Tour"`
}

// GetTours gets a list of all tours
//
// Note: This method will take several seconds, to minutes to return.
//       It is recommended to call this method in a goroutine and process the results.
func (c *Client) GetTours(ctx context.Context) ([]Tour, error) {
	reqURL := c.BaseURL

	reqURL.Path += "/Reference/ToursAndDepartures"

	req := &http.Request{
		URL:    &reqURL,
		Method: http.MethodGet,
	}

	var respData apiToursAndDeparturesQueryResponse

	err := c.RequestXML(ctx, req, &respData)

	return respData.Tours, err
}

// Duration gets the duration of a given departure
func (d *Departure) Duration() time.Duration {
	return d.End.Sub(d.Start.Time)
}
