package tms

import (
	"context"
	"testing"
)

func TestClient_GetImageTexts(t *testing.T) {

	client := getClient()
	text, err := client.GetImageTexts(context.TODO(), 3, "0-I-229", []string{"0-I-2161"})

	if err != nil {
		t.Error(err)
		t.Fail()
		return
	}

	t.Log(text)

}
