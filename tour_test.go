package tms

import (
	"context"
	"testing"
)

func TestClient_GetTours(t *testing.T) {

	// This is a long test!
	if testing.Short() {
		t.Skip()
	}

	c := getClient()

	tours, err := c.GetTours(context.TODO())

	if err != nil {
		t.Error("Error getting tours: ", err)
	}

	for _, tr := range tours {
		t.Logf("%s: %s", tr.Code, tr.Name)
	}
}
