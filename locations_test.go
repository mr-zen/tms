package tms

import (
	"context"
	"testing"
)

func TestClient_GetLocations(t *testing.T) {

	c := getClient()

	l, err := c.GetLocations(context.TODO())

	if err != nil {
		t.Error(err)
		t.Fail()
	}

	t.Log(l)
}
