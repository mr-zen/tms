package tms

import (
	"bytes"
	"context"
	"encoding/xml"
	"net/http"

	"github.com/aws/aws-xray-sdk-go/xray"
)

// CreateCustomerRequest is a request to create a new customer.
// It composes a Customer with a login Request
type CreateCustomerRequest struct {
	xml.Name `xml:"ApiCreateCustomerRequest"`
	Username string `xml:"UserName"`
	Password string `xml:"Password"`
	Customer
}

type CreateCustomerResponse struct {
	xml.Name `xml:"ApiCreateCustomerResponse"`
	Customer Customer `xml:"Customer"`
}

// CreateCustomer creates a new customer
func (c Client) CreateCustomer(ctx context.Context, params *CreateCustomerRequest) (*CreateCustomerResponse, error) {

	ctx, segment := xray.BeginSubsegment(ctx, "tms.Client.CreateCustomer")

	reqURL := c.BaseURL
	reqURL.Path += "/Authentication/CreateCustomer"

	var buffer []byte
	b := bytes.NewBuffer(buffer)

	if err := xml.NewEncoder(b).Encode(params); err != nil {
		segment.Close(err)
		return nil, err
	}

	req, err := http.NewRequest(http.MethodPost, reqURL.String(), b)

	if err != nil {
		segment.Close(err)
		return nil, err
	}

	resp := &CreateCustomerResponse{}

	if err := c.RequestXML(ctx, req, resp); err != nil {
		segment.Close(err)
		return nil, err
	}

	segment.Close(nil)
	return resp, nil
}
