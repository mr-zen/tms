package tms

import (
	"bytes"
	"context"
	"encoding/xml"
	"errors"
	"net/http"

	"github.com/aws/aws-xray-sdk-go/xray"
)

// LoginRequest represents a request body for the Login API
type LoginRequest struct {
	xml.Name `xml:"ApiAuthenticationCustomerRequest"`
	Username string `xml:"UserName"`
	Password string `xml:"Password"`
}

// LoginResponse represents the response to a Login action
type LoginResponse struct {
	xml.Name `xml:"ApiCustomerResponse"`
	Customer Customer `xml:"Customer"`
}

// CustomerAddress represents a physical customer address
type CustomerAddress struct {
	CountryCode string  `xml:"CountryCode"`
	State       string  `xml:"County"`
	Line1       string  `xml:"Line1"`
	Line2       *string `xml:"Line2"`
	Line3       *string `xml:"Line3"`
	PostalCode  *string `xml:"PostCode"`
	City        string  `xml:"Town"`
}

var (
	// ErrInvalidCredentials is returned when the user's credentials are invalid
	ErrInvalidCredentials = errors.New("invalid username or password")
)

// Login attempts to authenticate a customer
func (c Client) Login(ctx context.Context, loginReq *LoginRequest) (*LoginResponse, error) {

	ctx, segment := xray.BeginSubsegment(ctx, "tms.Client.Login")

	content, err := xml.MarshalIndent(loginReq, "", "\t")

	if err != nil {
		segment.Close(err)
		return nil, err
	}

	reqURL := c.BaseURL
	reqURL.Path += "/Authentication/AuthenticateCustomer"

	req, err := http.NewRequest(http.MethodPost, reqURL.String(), bytes.NewBuffer(content))

	req.Header.Set("Content-Type", "application/xml")

	if err != nil {
		segment.Close(err)
		return nil, err
	}

	res := &LoginResponse{}

	err = c.RequestXML(ctx, req, res)

	if err != nil {
		segment.Close(err)
		return nil, err
	}

	segment.Close(nil)
	return res, err
}
