package tms

import (
	"bytes"
	"context"
	"encoding/xml"
	"errors"
	"net/http"
)

// Customer represents a customer's details
type Customer struct {
	Username     string                                                       `xml:"UserName"`
	Title        string                                                       `xml:"Title"`
	FirstName    string                                                       `xml:"FirstName"`
	LastName     string                                                       `xml:"LastName"`
	Gender       Gender                                                       `xml:"Gender"`
	DateOfBirth  Zoneless                                                     `xml:"DateOfBirth"`
	Address      *CustomerAddress                                             `xml:"Address"`
	ContactsA    []CustomerContact                                            `xml:"ContactTypes>CustomerContactType,omitempty"`
	ContactsB    []CustomerContactsWithDifferentFieldNamesForNoApparentReason `xml:"ContactTypeDetails>ContactType,omitempty"`
	CustomerCode *string                                                      `xml:"CustomerCode,omitempty"`
}

type updateCustomerRequest struct {
	XMLName      xml.Name                                                     `xml:"Customer"`
	Username     string                                                       `xml:"UserName"`
	Title        string                                                       `xml:"Title"`
	FirstName    string                                                       `xml:"FirstName"`
	LastName     string                                                       `xml:"LastName"`
	Gender       Gender                                                       `xml:"Gender"`
	DateOfBirth  Zoneless                                                     `xml:"DateOfBirth"`
	Address      *CustomerAddress                                             `xml:"Address"`
	Contacts     []CustomerContactsWithDifferentFieldNamesForNoApparentReason `xml:"ContactTypeDetails>ContactType,omitempty"`
	CustomerCode *string                                                      `xml:"CustomerCode,omitempty"`
}

// Contact is a common interface to customer contact details which
// has multiple implementations
type Contact interface {
	Type() string
	Value() string
}

// CustomerContactsWithDifferentFieldNamesForNoApparentReason is a
// CustomerContact with different field names for no apparent reason.
type CustomerContactsWithDifferentFieldNamesForNoApparentReason struct {
	Typ string `xml:"Name"`
	Val string `xml:"Value"`
}

// CustomerContact is a CustomerContact
type CustomerContact struct {
	Typ string `xml:"ContactType"`
	Val string `xml:"Details"`
}

type apiContactTypesQueryResponse struct {
	xml.Name     `xml:"ApiContactTypesQueryResponse"`
	ContactTypes []string `xml:"ContactTypes>ContactType"`
}

// CustomerTitle represents a possible customer name title
type CustomerTitle struct {
	Name   string `xml:"Name"`
	Gender Gender `xml:"Gender"`
}

type apiPersonTitlesResponse struct {
	xml.Name `xml:"ApiPersonTitlesResponse"`
	Titles   []CustomerTitle `xml:"PersonTitles>PersonTitle"`
}

type forgotPasswordRequest struct {
	xmlName  xml.Name `xml:"ApiForgotCustomerPasswordRequest"`
	Username string   `xml:"UserName"`
}

type forgotPasswordResponse struct {
	xmlName xml.Name `xml:"ApiForgotPasswordResponse"`
	Token   string   `xml:"ForgottenToken"`
}

type changePasswordRequest struct {
	xmlName    xml.Name `xml:"ApiChangeCustomerPasswordRequest"`
	Username   string   `xml:"UserName"`
	ResetToken string   `xml:"ForgottenToken"`
	Password   string   `xml:"NewPassword"`
}

type changePasswordResponse struct {
	xmlName xml.Name `xml:"ApiChangeCustomerPasswordResponse"`
	Error   string   `xml:"Message"`
	Success bool     `xml:"IsSuccess"`
}

// GetContactTypes gets a list of all contact types.
func (c *Client) GetContactTypes(ctx context.Context) ([]string, error) {
	res := &apiContactTypesQueryResponse{}
	ru := c.BaseURL
	ru.Path += "/Reference/ContactTypes"

	r, _ := http.NewRequest(http.MethodGet, ru.String(), nil)

	err := c.RequestXML(ctx, r, res)

	return res.ContactTypes, err
}

// GetCustomerTitles gets a list of customer titles.
func (c *Client) GetCustomerTitles(ctx context.Context) ([]CustomerTitle, error) {
	res := &apiPersonTitlesResponse{}
	ru := c.BaseURL
	ru.Path += "/Reference/PersonTitles"

	r, _ := http.NewRequest(http.MethodGet, ru.String(), nil)

	err := c.RequestXML(ctx, r, res)

	return res.Titles, err
}

// GetCustomer gets a customer's details by id
func (c *Client) GetCustomer(ctx context.Context, customerCode string) (*Customer, error) {
	res := &Customer{}
	ru := c.BaseURL
	ru.Path += "/Authentication/Customers/" + customerCode

	r, _ := http.NewRequest(http.MethodGet, ru.String(), nil)

	err := c.RequestXML(ctx, r, res)

	return res, err
}

// UpdateCustomer updates an existing customer
func (c *Client) UpdateCustomer(ctx context.Context, customer *Customer) error {

	ru := c.BaseURL
	ru.Path += "/Authentication/Customers/" + *customer.CustomerCode

	uc := updateCustomerRequest{}
	uc.Username = customer.Username
	uc.Title = customer.Title
	uc.FirstName = customer.FirstName
	uc.LastName = customer.LastName
	uc.Gender = customer.Gender
	uc.DateOfBirth = customer.DateOfBirth
	uc.Address = customer.Address
	uc.CustomerCode = customer.CustomerCode

	uc.Contacts = make([]CustomerContactsWithDifferentFieldNamesForNoApparentReason, len(customer.Contacts()))

	for i, v := range customer.ContactsA {
		uc.Contacts[i] = CustomerContactsWithDifferentFieldNamesForNoApparentReason(v)
	}

	body, err := xml.MarshalIndent(uc, "", "\t")

	if err != nil {
		return err
	}

	r, _ := http.NewRequest(http.MethodPut, ru.String(), bytes.NewReader(body))

	return c.RequestXML(ctx, r, customer)
}

// GetResetToken gets a password reset token by username
func (c *Client) GetResetToken(ctx context.Context, username string) (*string, error) {

	req := forgotPasswordRequest{
		Username: username,
	}

	ru := c.BaseURL
	ru.Path += "/Authentication/ForgotCustomerPassword"

	body, err := xml.MarshalIndent(req, "", "\t")

	if err != nil {
		return nil, err
	}

	r, _ := http.NewRequest(http.MethodPost, ru.String(), bytes.NewReader(body))

	resp := forgotPasswordResponse{}

	if err := c.RequestXML(ctx, r, &resp); err != nil {
		return nil, err
	}

	return &resp.Token, nil
}

// ChangePassword changes a user's password
func (c *Client) ChangePassword(ctx context.Context, username, token, password string) error {

	req := changePasswordRequest{
		Username:   username,
		ResetToken: token,
		Password:   password,
	}

	ru := c.BaseURL
	ru.Path += "/Authentication/ChangeCustomerPassword"

	body, err := xml.MarshalIndent(req, "", "\t")

	if err != nil {
		return err
	}

	r, _ := http.NewRequest(http.MethodPost, ru.String(), bytes.NewReader(body))

	resp := changePasswordResponse{}

	if err := c.RequestXML(ctx, r, &resp); err != nil {
		return err
	}

	if resp.Success {
		return nil
	}

	return errors.New(resp.Error)
}

// Type gets the contact type for a CustomerContact
func (cc CustomerContact) Type() string {
	return cc.Typ
}

// Value gets the value of a CustomerContact
func (cc CustomerContact) Value() string {
	return cc.Val
}

// Type gets the contact type for a CustomerContactsWithDifferentFieldNamesForNoApparentReason
func (cc CustomerContactsWithDifferentFieldNamesForNoApparentReason) Type() string {
	return cc.Typ
}

// Value gets the value of a CustomerContactsWithDifferentFieldNamesForNoApparentReason
func (cc CustomerContactsWithDifferentFieldNamesForNoApparentReason) Value() string {
	return cc.Val
}

// Contacts gets the customer's contacts
func (c *Customer) Contacts() []Contact {
	if len(c.ContactsB) > 0 {
		return func(c []CustomerContactsWithDifferentFieldNamesForNoApparentReason) []Contact {
			v := make([]Contact, len(c))

			for i, a := range c {
				v[i] = a
			}

			return v
		}(c.ContactsB)
	}

	if len(c.ContactsA) > 0 {
		return func(c []CustomerContact) []Contact {
			v := make([]Contact, len(c))

			for i, a := range c {
				v[i] = a
			}

			return v
		}(c.ContactsA)
	}

	return []Contact{}
}
