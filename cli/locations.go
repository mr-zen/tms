package main

import (
	"context"
	"github.com/aws/aws-xray-sdk-go/xray"
	"github.com/spf13/viper"
	"github.com/urfave/cli"
	"fmt"
	"strings"
)

const (
	width = 60
	indent = 2
)


func locations(c *cli.Context) error {

	client := tmsClient()

	ctx := context.WithValue(context.Background(), "command", c.Command.Name)

	if viper.GetBool("xray.enabled") {
		var segment *xray.Segment
		ctx, segment = xray.BeginSegment(ctx, "mrzen/tms/cli")

		segment.AddAnnotation("command", c.Command.Name)

		defer segment.Close(nil)
	}


	regions, err := client.GetLocations(ctx)

	if err != nil {
		return err
	}

	for _, r := range regions {
		fmt.Println(
			r.Name,
			strings.Repeat(".", (width+1) - len(r.Name)),
			r.Code,
			strings.Repeat(" ", 10 - len(r.Code)),
			"R",
		)

		for _, c := range r.Countries {
			fmt.Println(
				strings.Repeat(" ", indent),
				c.Name,
				strings.Repeat(".", width - indent - len(c.Name)),
				c.Code,
				strings.Repeat(" ", 10 - len(c.Code)),
				"C",
			)
			for _, s := range c.Resorts {
				fmt.Println(
					strings.Repeat(" ", 2*indent),
					s.Name,
					strings.Repeat(".", width - 2*indent - len(s.Name)),
					s.Code,
					strings.Repeat(" ", 10 - len(s.Code)),
					"S",
				)

				for _, a := range s.Airports {
					fmt.Println(
						strings.Repeat(" ", 3*indent),
						a.Name,
						strings.Repeat(".", width - 3*indent - len(a.Name)),
						a.Code,
						strings.Repeat(" ", 10 - len(a.Code)),
						"A",
					)
				}

				for _, h := range s.Hotels {

					if len(h.Name) > (width - 3*indent - 3) {
						h.Name = h.Name[:29] + "…"
					}

					fmt.Println(
						strings.Repeat(" ", 3*indent),
						h.Name,
						strings.Repeat(".", width - 3*indent - len(h.Name)),
						h.Code,
						strings.Repeat(" ", 10 - len(h.Code)),
						"H",
					)
				}
			}
		}
	}

	return nil
}