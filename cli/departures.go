package main

import (
	"context"
	"fmt"
	"os"
	"strconv"
	"strings"
	"text/tabwriter"
	"time"

	"github.com/briandowns/spinner"
	"github.com/urfave/cli"
)

func departures(c *cli.Context) error {

	ctx := context.Background()

	client := tmsClient()

	sp := spinner.New(spinner.CharSets[14], 100*time.Millisecond)
	sp.Writer = os.Stderr
	sp.Suffix = "Loading Data from TMS"
	sp.Start()

	tours, err := client.GetTours(ctx)

	sp.Stop()

	if err != nil {
		return err
	}

	tw := tabwriter.NewWriter(c.App.Writer, 2, 2, 2, byte(' '), 0)

	tw.Write([]byte("Code\tName\tStart\tEnd\tDuration\tSpaces\tPrice\tLink" + "\n"))

	for _, t := range tours {

		for _, d := range t.Departures {
			fields := []string{
				t.Code,
				t.Name,
				d.Start.Format("2006-01-02"),
				d.End.Format("2006-01-02"),
				fmt.Sprintf("%.0f", d.Duration().Truncate(time.Hour).Hours()/24),
				strconv.Itoa(int(d.Spaces)),
				fmt.Sprintf("£%.2f", d.Price),
				fmt.Sprintf("https://www.solosholidays.co.uk/booking?code=%s&start=%s&end=%s", t.Code, d.Start.Format("2006-01-02"), d.End.Format("2006-01-02")),
			}

			tw.Write([]byte(strings.Join(fields, "\t") + "\n"))
		}
	}

	tw.Flush()

	return nil
}
