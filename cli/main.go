package main

import (
	"net/url"
	"os"
	"time"

	"bitbucket.org/mr-zen/tms"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"github.com/urfave/cli"
)

var app *cli.App

func init() {
	app = cli.NewApp()
	app.Name = "TMS Tester"
	app.Description = "A CLI tool for testing TMS API client."
	app.Authors = []cli.Author{
		{
			Name:  "Leo Adamek",
			Email: "leo.adamek@mrzen.com",
		},
	}

	app.Flags = []cli.Flag{
		cli.BoolFlag{
			Name:  "debug",
			Usage: "Enable debugging.",
		},
	}

	app.Commands = []cli.Command{
		{
			Name:   "locations",
			Usage:  "List out all locations",
			Action: locations,
		},
		{
			Name:   "departures",
			Usage:  "List departures",
			Action: departures,
		},

		{
			Name:  "reference",
			Usage: "List Reference Data",
			Subcommands: []cli.Command{
				{
					Name:   "contact-types",
					Action: contactTypes,
					Usage:  "List contact types",
				},

				{
					Name:   "titles",
					Action: customerTitles,
					Usage:  "List customer titles",
				},

				{
					Name:   "airlines",
					Action: airlines,
					Usage:  "List Airlines",
				},

				{
					Name:      "categories",
					Action:    categories,
					Usage:     "List Categories",
					UsageText: "<type>",
				},
			},
		},
		{
			Name:    "availabilty",
			Aliases: []string{"av"},
			Subcommands: []cli.Command{
				{
					Name:   "fit",
					Action: fitAvailability,
					Flags: []cli.Flag{
						cli.StringFlag{
							Name:  "country-code",
							Usage: "Country Code",
						},

						cli.StringFlag{
							Name:  "hotel-code",
							Usage: "Hotel Code",
						},

						cli.UintFlag{
							Name:  "n",
							Usage: "Nights",
							Value: 3,
						},

						cli.StringFlag{
							Name:  "d",
							Usage: "Start Date",
							Value: time.Now().Format("2006-01-02"),
						},

						cli.BoolFlag{
							Name:  "f",
							Usage: "Include Flights?",
						},

						cli.UintFlag{
							Name:  "p",
							Usage: "Number of passengers",
							Value: 1,
						},
					},
				},
			},
		},
	}
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:  "c",
			Usage: "Configuration File",
		},
	}

	app.Before = func(c *cli.Context) error {
		if path := c.String("c"); path != "" {
			viper.SetConfigFile(path)
			return viper.ReadInConfig()
		}

		return nil
	}

	viper.SetDefault("tms.url", "https://services.anteeo.co.uk/SolosTest/TMSWebAPI")
	viper.SetDefault("tms.username", "MrZen_Solos")
	viper.SetDefault("tms.password", "0X-xsh-TBXdK")
	viper.SetDefault("tms.timeout", 600*time.Second)
	viper.SetDefault("tms.debug", false)

	viper.BindEnv("tms.url", "TMS_URL")
	viper.BindEnv("tms.username", "TMS_USER")
	viper.BindEnv("tms.debug", "TMS_DEBUG")
}

func tmsClient() tms.Client {
	rawUrl := viper.GetString("tms.url")
	u, err := url.Parse(rawUrl)

	if err != nil {
		logrus.WithError(err).Fatalln("Unable to parse endpoint URL: ", err)
	}

	logrus.WithFields(logrus.Fields{
		"url":      rawUrl,
		"username": viper.GetString("tms.username"),
		"password": viper.GetString("tms.password"),
		"timeout":  viper.GetDuration("tms.timeout"),
	}).Debugln("Creating TMS Client")

	c := tms.NewClient(
		u,
		viper.GetString("tms.username"),
		viper.GetString("tms.password"),
		viper.GetDuration("tms.timeout"),
	)

	if viper.GetBool("debug") {
		c.Logger.Level = logrus.DebugLevel
	}

	return c
}

func main() {

	if err := app.Run(os.Args); err != nil {
		logrus.Fatalln("Application Error:", err)
	}
}
