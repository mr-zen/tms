package main

import (
	"encoding/xml"
	"os"
	"time"

	"bitbucket.org/mr-zen/tms"
	"github.com/urfave/cli"
)

var (
	// DefaultDateOfBirth is the default Date of Birth for an unknown user.
	DefaultDateOfBirth = time.Now().AddDate(-30, 0, -1).Truncate(24 * time.Hour).In(time.UTC)

	// DefaultAge is the default age used for an unknown user
	DefaultAge uint = 30
)

func fitAvailability(c *cli.Context) error {

	//client := tmsClient()

	sd := c.String("d")

	start, err := time.Parse("2006-01-02", sd)

	if err != nil {
		return err
	}

	pax := c.Uint("p")

	guests := make([]tms.RoomGuest, pax)

	for i := uint(0); i < pax; i++ {
		guests[i] = tms.RoomGuest{
			Age:    DefaultAge,
			Gender: tms.GenderUnknown,
		}
	}

	req := tms.FITAvailabilityRequest{
		IncludeGrounds: false,
		IncludeHotels:  true,
		IncludeFlights: true,
		IncludeTours:   false,
		Country:        c.String("country-code"),
		Hotel: &tms.HotelAvailabilityRequest{

			HotelCode: c.String("hotel-code"),
			Nights:    c.Uint("n"),
			StartDate: start,
			Rooms: []tms.Rooming{
				{
					Guests: guests,
				},
			},
		},
	}
	wr := os.Stdout
	enc := xml.NewEncoder(wr)
	enc.Indent("", "\t")
	enc.Encode(req)

	return nil
}
