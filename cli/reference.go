package main

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"text/tabwriter"

	"github.com/aws/aws-xray-sdk-go/xray"
	"github.com/urfave/cli"
)

func contactTypes(c *cli.Context) error {
	client := tmsClient()

	ctx := context.Background()

	contacts, err := client.GetContactTypes(ctx)

	if err != nil {
		return err
	}

	for _, c := range contacts {
		fmt.Println(c)
	}

	return nil
}

func customerTitles(c *cli.Context) error {
	client := tmsClient()

	ctx := context.Background()

	titles, err := client.GetCustomerTitles(ctx)

	if err != nil {
		return err
	}

	tw := tabwriter.NewWriter(c.App.Writer, 2, 2, 2, byte(' '), 0)

	for _, c := range titles {
		tw.Write([]byte(c.Name + "\t" + string(c.Gender) + "\n"))
	}

	tw.Flush()

	return nil
}

func categories(c *cli.Context) error {
	client := tmsClient()

	ctx := context.Background()

	ctx, _ = xray.BeginSegment(ctx, "cli/categories")

	objectType := c.Args().First()

	if objectType == "" {
		return errors.New("object type is required")
	}

	categories, err := client.ListCategories(ctx, objectType)

	if err != nil {
		return err
	}

	tw := tabwriter.NewWriter(c.App.Writer, 2, 2, 2, byte(' '), 0)

	sb := new(bytes.Buffer)

	for _, c := range categories {
		sb.WriteString(c.Code)
		sb.WriteString("\t")
		sb.WriteString(c.Name)
		sb.WriteString("\n")

		for _, t := range c.Characteristics {
			sb.WriteString(" → ")
			sb.WriteString(t.Code)
			sb.WriteString("\t")
			sb.WriteString(t.Name)
			sb.WriteString("\n")
		}

		tw.Write(sb.Bytes())
	}

	return tw.Flush()

}
