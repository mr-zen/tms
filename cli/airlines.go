package main

import (
	"bytes"
	"context"
	"text/tabwriter"

	"github.com/urfave/cli"
)

func airlines(c *cli.Context) error {

	ctx := context.Background()

	client := tmsClient()

	airlines, err := client.GetAirlines(ctx)

	if err != nil {
		return err
	}

	tw := tabwriter.NewWriter(c.App.Writer, 2, 2, 2, byte(' '), 0)

	tw.Write([]byte("Airline Code\tCode\tName\tDescription\tFlags\n"))

	for _, line := range airlines {
		content := new(bytes.Buffer)
		content.WriteString(line.TMSCode)
		content.WriteByte('\t')

		if line.IATACode == nil || *line.IATACode == "" {
			content.WriteString("<null>")
		} else {
			content.WriteString(*line.IATACode)
		}

		content.WriteByte('\t')
		content.WriteString(line.Name)
		content.WriteByte('\t')
		content.WriteString(line.Descrition)
		content.WriteByte('\n')

		tw.Write(content.Bytes())
	}

	tw.Flush()
	return nil
}
