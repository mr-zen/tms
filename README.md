TMS
===

A Go library for interacting with TMS by [Anteeo](https://anteeo.co.uk)

Features
--------

* Customizable Timeouts
* Configurable/Overridable Logging
* Configurable/Overridable Transport


Diagnostic Logs
---------------

By setting the viper configuration value `tms.api_log` to a file path, you can
get a full log of all HTTP requests/responses, compressed with gzip.


Packages
--------

### `./cli`

Provides a command-line client for performing ad-hoc commands to query system data.
