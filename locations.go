package tms

import (
	"context"
	"encoding/xml"
	"net/http"
)

type Locations struct {
	XMLName xml.Name `xml:"ApiHotelAndLocationQueryResponse"`
	Regions []Region `xml:"Regions>Region"`
}

type Region struct {
	XMLName   xml.Name `xml:"Region"`
	Name      string
	Code      string    `xml:"RegionCode"`
	Countries []Country `xml:"Countries>Country"`
}

type Country struct {
	XMLName xml.Name `xml:"Country"`
	Code    string   `xml:"CountryCode"`
	Name    string

	Resorts []Resort `xml:"Resorts>Resort"`
}

type Resort struct {
	XMLName  xml.Name `xml:"Resort"`
	Code     string   `xml:"ResortCode"`
	Name     string
	Hotels   []Hotel   `xml:"Hotels>Hotel"`
	Airports []Airport `xml:"Airports>Airport"`
}

type Airport struct {
	Name string
	Code string `xml:"AirportCode"`
}

type Hotel struct {
	Name string
	Code string `xml:"HotelCode"`
}

func (c Client) GetLocations(ctx context.Context) ([]Region, error) {

	res := &Locations{}
	ru := c.BaseURL
	ru.Path += "/Reference/HotelAndLocationData"
	ru.RawQuery = "flags=1024"

	r, _ := http.NewRequest(http.MethodGet, ru.String(), nil)

	err := c.RequestXML(ctx, r, res)

	return res.Regions, err

}
