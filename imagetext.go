package tms

import (
	"bytes"
	"context"
	"encoding/xml"
	"net/http"
)

// ImageText represents a single named item of image or textual content relating
// to another datum
type ImageText struct {
	ID          int    `xml:"ID"`
	Name        string `xml:"Name"`
	Title       string `xml:"Title"`
	Order       int    `xml:"OrderNumber"`
	Description string `xml:"Description"`
	LongText    string `xml:"LongText"`
	ShortText   string `xml:"ShortText"`
	ImageURL    string `xml:"ImageURL"`
	URL         string `xml:"URL"`
}

// ApiGetImageTextsRequest represents a request to get ImageText data
type ApiGetImageTextsRequest struct {
	BrandID   int      `xml:"BrandID"`
	TypeCode  string   `xml:"ObjectCode"`
	ItemCodes []string `xml:"ItemCodes>ItemCode"`
}

type ApiGetImageTextsResponse struct {
	ImageTexts []ImageText `xml:"ImageTexts>ImageText"`
}

func (c Client) GetImageTexts(ctx context.Context, brandID int, typeCode string, itemCodes []string) ([]ImageText, error) {
	params := ApiGetImageTextsRequest{
		BrandID:   brandID,
		TypeCode:  typeCode,
		ItemCodes: itemCodes,
	}

	body, _ := xml.Marshal(params)

	reqUrl := c.BaseURL
	reqUrl.Path += "/CMS/ImageTexts"
	req, err := http.NewRequest(http.MethodPost, reqUrl.String(), bytes.NewReader(body))

	if err != nil {
		return []ImageText{}, err
	}

	var res ApiGetImageTextsResponse

	err = c.RequestXML(ctx, req, &res)

	c.Logger.Debugf("%#+v", res)

	return res.ImageTexts, err
}
