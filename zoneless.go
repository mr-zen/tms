package tms

import (
	"encoding/xml"
	"time"
)

// Zoneless implements a timestamp without time-zone
type Zoneless struct {
	time.Time
}

// ZonelessFormat defines the text format of a Zoneless timestamp
const ZonelessFormat = "2006-01-02T15:04:05"

// UnmarshalXML decodes an XML element containing Zonless data.
func (z *Zoneless) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	var v string

	d.DecodeElement(&v, &start)

	parse, err := time.Parse(ZonelessFormat, v)

	if err != nil {
		return err
	}

	*z = Zoneless{parse}
	return nil
}

func (z Zoneless) MarshalXML(e *xml.Encoder, start xml.StartElement) error {
	value := z.Format(ZonelessFormat)

	return e.EncodeElement(value, start)
}
