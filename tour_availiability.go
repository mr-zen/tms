package tms

import (
	"bytes"
	"context"
	"encoding/xml"
	"net/http"
	"time"

	"github.com/aws/aws-xray-sdk-go/xray"
)

// TourAvailabilitySearchRequest is the request body for a tour availability search
type TourAvailabilitySearchRequest struct {
	XMLName xml.Name `xml:"ApiGetTourItemsAvailabilityRequest"`

	Code      string    `xml:"TourCode"`
	Departure time.Time `xml:"DepartureDate"`
	GroupSize int       `xml:"GroupSize"`
	Rooms     []Rooming `xml:"Rooms>Room"`
}

// TourAvailabiltySearchResponse represents the response to a tour availability search
type TourAvailabiltySearchResponse struct {
	xml.Name `xml:"ApiGetTourItemsAvailabilityQueryResponse"`
	Flights  []Flight            `xml:"Flights>Flight"`
	Hotels   []HotelAvailability `xml:"Hotels>ResponseHotel"`
	Grounds  []GroundService     `xml:"Grounds>GroundItem"`
}

// GetTourAvailability checks availability for a specific tour.
func (c *Client) GetTourAvailability(ctx context.Context, req *TourAvailabilitySearchRequest) (*TourAvailabiltySearchResponse, error) {

	ctx, seg := xray.BeginSubsegment(ctx, "tms.Client.GetTourAvailability")

	if seg != nil {
		seg.AddMetadataToNamespace("tms", "getTourAvailability", map[string]interface{}{
			"tour_code":  req.Code,
			"departure":  req.Departure,
			"group_size": req.GroupSize,
		})
	}

	reqURL := c.BaseURL
	reqURL.Path += "/Availability/TourItemSearch"

	content, err := xml.MarshalIndent(req, "", "\t")

	if err != nil {
		seg.Close(err)
		return nil, err
	}

	httpreq, err := http.NewRequest(http.MethodPost, reqURL.String(), bytes.NewReader(content))

	if err != nil {
		seg.Close(err)
		return nil, err
	}

	rsp := &TourAvailabiltySearchResponse{}

	if err := c.RequestXML(ctx, httpreq, rsp); err != nil {
		seg.Close(err)
		return nil, err
	}

	seg.Close(nil)
	return rsp, nil
}
