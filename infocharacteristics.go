package tms

import (
	"bytes"
	"context"
	"encoding/xml"
	"net/http"
)

// UpdateInfoCharacteristicDetailsRequest is the API request data to update
// the Information characteristics for an item.
type UpdateInfoCharacteristicDetailsRequest struct {
	ObjectCode string                `xml:"ObjectCode"`
	ItemCode   string                `xml:"ItemCode"`
	Categories []InformationCategory `xml:"Categories>UpdateInfoCharacteristicDetailsCategory"`
}

type InformationCategory struct {
	Name    string `xml:"Name"`
	Details struct {
		Name   string   `xml:"Characteristic"`
		Values []string `xml:"Values>Value"`
	} `xml:"Details>UpdateInfoCharacteristicDetailsDetail"`
}

// CategoryDefinition is the definition for an
// information characteristic category
type CategoryDefinition struct {
	Code string `xml:"InfoCategoryCode"`
	Name string `xml:"Name"`

	Characteristics []struct {
		Code string `xml:"InfoCharacteristicCode"`
		Name string `xml:"Name"`
	} `xml:"Characteristics>InfoCharacteristic"`
}

type categoryListResponse struct {
	XMLName    xml.Name             `xml:"ApiInfoCategoriesResponse"`
	Categories []CategoryDefinition `xml:"InfoCategories>InfoCategory"`
}

// UpdateCharacteristics updates the characteristics of an item.
func (c *Client) UpdateCharacteristics(ctx context.Context, params *UpdateInfoCharacteristicDetailsRequest) (bool, error) {

	body, err := xml.MarshalIndent(params, "", "\t")

	if err != nil {
		return false, err
	}

	reqURL := c.BaseURL
	reqURL.Path += "/CMS/InfoCharacteristicDetails/Update"

	req, _ := http.NewRequest(http.MethodPost, reqURL.String(), bytes.NewReader(body))

	req.Header.Set("Content-Type", "text/xml")
	req.Header.Set("Accept", "text/xml")
	req.Header.Add("Accept", "text/*")

	res, err := c.Request(ctx, req)

	if err != nil {
		return false, err
	}

	return res.StatusCode >= 200 && res.StatusCode < 300, nil
}

// ListCategories lists the Information categories for a given type
func (c *Client) ListCategories(ctx context.Context, ObjectType string) ([]CategoryDefinition, error) {
	reqURL := c.BaseURL
	reqURL.Path += "/CMS/InfoCategories"
	q := reqURL.Query()
	q.Set("ObjectName", ObjectType)

	reqURL.RawQuery = q.Encode()

	req, _ := http.NewRequest(http.MethodGet, reqURL.String(), nil)

	req.Header.Set("Accept", "text/xml")
	req.Header.Add("Accept", "text/*")

	res := &categoryListResponse{}

	err := c.RequestXML(ctx, req, &res)

	if err != nil {
		return nil, err
	}

	return res.Categories, nil
}
