package tms

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	durationHistogram *prometheus.HistogramVec
)

func init() {
	durationHistogram = promauto.NewHistogramVec(prometheus.HistogramOpts{
		Namespace: "tms_client",
		Name:      "request_duration_seconds",
		Help:      "Client Request Duration",
		Buckets:   []float64{0.01, 0.1, 0.5, 1.0, 5.0, 15.0},
	}, []string{"method", "path", "status"})
}
