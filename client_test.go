package tms

import (
	"context"
	"crypto/tls"
	"net/http"
	"net/url"
	"testing"
	"time"

	"github.com/aws/aws-xray-sdk-go/strategy/ctxmissing"
	"github.com/aws/aws-xray-sdk-go/xray"
	"github.com/seborama/govcr"
	"github.com/sirupsen/logrus"
)

func init() {

	cfg := xray.Config{
		ContextMissingStrategy: ctxmissing.NewDefaultLogErrorStrategy(),
	}

	xray.Configure(cfg)
}

func getClient() Client {

	url, _ := url.Parse("https://services.anteeo.co.uk/SolosTest/TMSWebAPI")

	c := NewClient(
		url,
		"MrZen_Solos",
		"0X-xsh-TBXdK",
		600*time.Second,
	)

	mockTransport := http.DefaultTransport.(*http.Transport)

	mockTransport.TLSClientConfig = &tls.Config{
		InsecureSkipVerify: true,
	}

	c.HttpClient.Transport = mockTransport

	vcr := govcr.NewVCR("client-vcr", &govcr.VCRConfig{
		Client: c.HttpClient,
	})

	c.HttpClient = vcr.Client

	c.Logger.Level = logrus.DebugLevel

	return c

}

func TestClient_GetToken(t *testing.T) {
	c := getClient()

	token, expiry, err := c.GetToken(context.TODO())

	if err != nil {
		t.Errorf("Got error retriving token: %s", err)
	}

	t.Log("Token expires at ", expiry, " content is ", token)
}
