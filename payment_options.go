package tms

import (
	"bytes"
	"context"
	"encoding/xml"
	"net/http"
)

// PaymentOptionsRequest is identical to CreateReservation request, almost.
type PaymentOptionsRequest struct {
	XMLName xml.Name `xml:"ApiPaymentOptionsPostRequest"`

	Booking Booking `xml:"Booking"`
}

// PaymentOptionsResponse is the response data to a PaymentOptions request.
type PaymentOptionsResponse struct {
	CardTypes []struct {
		ID           uint64  `xml:"PaymentTypeId"`
		Name         string  `xml:"Name"`
		CardFeeType  string  `xml:"CardFeeType"`
		CardFeeValue float64 `xml:"CardFeeValue"`
	} `xml:"CardTypes>CardType"`

	Options []struct {
		Type         string  `xml:"Type"`
		Value        float64 `xml:"Amount"`
		CurrencyCode string  `xml:"CurrencyCode"`
	} `xml:"Options>PaymentAmount"`

	BalanceDue string `xml:"BalanceDue"`
}

// PaymentOptions gets the available payment options for a given reservation
func (c *Client) PaymentOptions(ctx context.Context, req *PaymentOptionsRequest) (*PaymentOptionsResponse, error) {

	reqURL := c.BaseURL
	reqURL.Path += "/Payment/PaymentOptions"

	content, err := xml.MarshalIndent(req, "", "\t")

	if err != nil {
		return nil, err
	}

	httpreq, err := http.NewRequest(http.MethodPost, reqURL.String(), bytes.NewReader(content))

	if err != nil {
		return nil, err
	}

	rsp := &PaymentOptionsResponse{}

	if err := c.RequestXML(ctx, httpreq, rsp); err != nil {
		return nil, err
	}

	return rsp, nil
}
