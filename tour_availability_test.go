package tms

import (
	"context"
	"testing"
	"time"
)

func TestClient_GetTourAvailability(t *testing.T) {

	c := getClient()

	ctx := context.Background()

	req := &TourAvailabilitySearchRequest{
		Code:      "25-I-4680",
		Departure: time.Date(2019, 12, 30, 0, 0, 0, 0, time.UTC),
		GroupSize: 1,
		Rooms: []Rooming{
			{
				ID: 1,
				Guests: []RoomGuest{
					{
						Gender: GenderUnknown,
						Age:    30,
					},
				},
			},
		},
	}

	res, err := c.GetTourAvailability(ctx, req)

	if err != nil {
		t.Error(err)
		return
	}

	t.Logf("%+#v", res)

}
