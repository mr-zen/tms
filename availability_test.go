package tms

import (
	"context"
	"testing"
)

func TestClient_GetAvailability(t *testing.T) {
	testCode := "25-I-3961"

	client := getClient()

	av, err := client.GetAvailability(context.TODO(), testCode)

	if err != nil {
		t.Error("Unable to get availability:", err)
		t.FailNow()
	}

	t.Logf("%+v", av)

}

func TestClient_GetFITAvailability(t *testing.T) {

}
