package tms

// FlightLeg represents a single leg of a specific flight.
type FlightLeg struct {
	ArrivalAirportCode   string
	ArrivalAirportName   string
	ArrivalDate          Zoneless
	ArrivalTerminal      *string
	ArrivalTime          Zoneless
	Baggage              string
	CabinCode            string
	CarrierCode          string
	CarrierName          string
	CheckInTime          Zoneless
	ClassCode            *string
	ClassName            string
	DepartureAirportCode string
	DepartureAirportName string
	DepartureDate        Zoneless
	DepartureTerminal    *string
	DepartureTime        Zoneless
	FareType             int
	FlightLegOrder       int
	FlightNumber         string
	MarketingCarrierCode string
	Meals                string
	NumTechStops         int
	SpecialRequest       string
}

// FlightRoute represents a route taken for a flight package
// which comprises multiple leg segments
type FlightRoute []FlightLeg

func (fr FlightRoute) Len() int {
	return len([]FlightLeg(fr))
}

func (fr FlightRoute) Less(i, j int) bool {
	l := []FlightLeg(fr)

	a := l[i]
	b := l[j]

	return a.DepartureTime.Time.Before(b.DepartureTime.Time)
}

func (fr FlightRoute) Swap(i, j int) {

	r := []FlightLeg(fr)

	r[i], r[j] = r[j], r[i]

	fr = FlightRoute(r)
}
