package tms

import (
	"bytes"
	"context"
	"encoding/xml"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"runtime"
	"strconv"
	"strings"
	"time"

	"github.com/aws/aws-xray-sdk-go/xray"
	"github.com/spf13/viper"
	"golang.org/x/net/context/ctxhttp"

	"github.com/sirupsen/logrus"
)

// UserAgent is the default User-Agent header to use
var UserAgent = fmt.Sprintf("TMSClient/1.0(os='%s',arch='%s')", runtime.GOOS, runtime.GOARCH)

// Client represents an API client for the TMS API
type Client struct {
	// Base URL for the API
	BaseURL url.URL

	// API Username
	Username string

	// API Password
	Password string

	// Authentication Token
	Token string

	// Token Expiry Time
	TokenExpiry time.Time

	// Underlying HTTP Client
	HttpClient *http.Client

	// Logger
	Logger *logrus.Logger
}

// NewClient creates a new API client.
func NewClient(baseURL *url.URL, username, password string, timeout time.Duration) Client {

	if timeout == 0 {
		timeout = 30 * time.Second
	}

	c := &http.Client{
		Timeout: timeout,
	}

	if viper.GetBool("xray.enabled") {
		c = xray.Client(c)
		xray.BeginSegment(context.Background(), baseURL.Hostname())
	}

	client := Client{
		BaseURL:     *baseURL,
		Username:    username,
		Password:    password,
		HttpClient:  c,
		TokenExpiry: time.Unix(0, 0),
		Logger:      logrus.New(),
	}

	return client
}

// RequestXML makes an XML Request and decodes the result into
// the given interface.
//
// Parameters:
//   req - Request object
//   into - Value to decode response into
func (c *Client) RequestXML(ctx context.Context, req *http.Request, into interface{}) error {

	if req.Header == nil {
		req.Header = http.Header{}
	}

	req.Header.Set("Content-Type", "text/xml")
	req.Header.Set("Accept", "text/xml")

	resp, err := c.Request(ctx, req)

	if err != nil {
		c.Logger.WithError(err).Errorln("Error making request")
		return err
	}

	buf := new(bytes.Buffer)
	tee := io.TeeReader(resp.Body, buf)

	err = xml.NewDecoder(tee).Decode(into)

	if err != nil {
		c.Logger.WithError(err).WithField("data", buf.String()).Errorln("Unable to unmarshal response:", err)
	}

	return err
}

// Request makes an HTTP request on the client.
//
// Parameters:
//
//   * req - Request object
func (c *Client) Request(ctx context.Context, req *http.Request) (*http.Response, error) {

	// Check if the auth token is valid
	// If not, update it.
	if c.TokenExpiry.Before(time.Now()) {
		token, expiry, err := c.GetToken(ctx)

		if err != nil {
			return nil, err
		}

		c.Token = token
		c.TokenExpiry = expiry
	}

	var cr io.WriteCloser

	if logPath := viper.GetString("tms.log_path"); logPath != "" {
		tmpFile, err := os.OpenFile(logPath, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
		defer tmpFile.Close()
		if err == nil {

			cr = tmpFile

			cr.Write([]byte("\n"))
			fmt.Fprintln(cr, strings.Repeat("-", 80))
			fmt.Fprintln(cr, time.Now().Format(time.RFC3339)+" "+req.Method+" "+req.URL.String())

			req.Header.Write(cr)

			if req.Body != nil {
				cr.Write([]byte("\n"))
				if c.RequestIsLoggable(req) {
					buf := new(bytes.Buffer)
					tee := io.TeeReader(req.Body, buf)
					io.Copy(cr, tee)
					req.Body = ioutil.NopCloser(buf)
				} else {
					fmt.Fprintln(cr, "** Request body not logged **")
				}
			}

		}
	}

	req.Header.Set("User-Agent", UserAgent)
	req.Header.Set("Authorization", "TMS "+c.Token)

	st := time.Now()

	resp, err := ctxhttp.Do(ctx, c.HttpClient, req)

	if err != nil {
		return nil, err
	}

	dt := time.Now().Sub(st)

	durationHistogram.WithLabelValues(
		req.Method,
		req.URL.Path,
		strconv.Itoa(resp.StatusCode),
	).Observe(dt.Seconds())

	c.Logger.WithFields(logrus.Fields{
		"method": req.Method,
		"url":    req.URL.String(),
		"length": resp.ContentLength,
		"Δ":      dt,
	}).Debugf("Request: %s %s", req.Method, req.URL.String())

	buf := new(bytes.Buffer)
	tee := io.TeeReader(resp.Body, buf)

	if cr != nil {
		cr.Write([]byte("\n\n"))
		cr.Write([]byte(resp.Status + "\n"))
		resp.Header.Write(cr)
		io.Copy(cr, tee)
		resp.Body = ioutil.NopCloser(buf)
	}

	if resp.StatusCode >= 400 {

		body := buf.Bytes()

		if err != nil {
			return nil, err
		}

		errResp := &ValidationErrorResponse{}
		segment := xray.GetSegment(ctx)

		if err := xml.Unmarshal(body, errResp); err == nil {
			segment.AddError(errResp)
			segment.Error = true
			segment.Fault = false
			segment.Close(nil)
			return nil, errResp
		}

		err = errors.New(string(body))
		segment.Close(err)
		return nil, err
	}

	return resp, err
}

// GetToken gets an authentication token and returns it with its expiry time.
func (c *Client) GetToken(ctx context.Context) (string, time.Time, error) {

	reqURL := c.BaseURL
	reqURL.Path += "/Authentication/Get"

	q := reqURL.Query()
	q.Set("userName", c.Username)
	q.Set("password", c.Password)
	reqURL.RawQuery = q.Encode()

	headers := http.Header{}
	headers.Set("User-Agent", UserAgent)
	headers.Set("Accept", "text/plain")

	req := &http.Request{
		Method: http.MethodGet,
		URL:    &reqURL,
		Header: headers,
	}

	c.Logger.WithFields(logrus.Fields{
		"url":     reqURL.String(),
		"headers": headers,
	}).Debugf("Request: %s %s", req.Method, req.URL.String())

	res, err := ctxhttp.Do(ctx, c.HttpClient, req)

	if err != nil {
		return "", time.Unix(0, 0), err
	}

	if res.StatusCode >= 400 {
		logrus.WithFields(logrus.Fields{
			"status":      res.Status,
			"status_code": res.StatusCode,
		}).Errorln("Got an error while trying to get token.")
		return "", time.Unix(0, 0), errors.New("response error")
	}

	tokenBytes, err := ioutil.ReadAll(res.Body)

	if err != nil {
		return "", time.Unix(0, 0), err
	}

	expiry := time.Now().Add(2 * time.Hour)

	return string(tokenBytes), expiry, nil
}

// RequestIsLoggable determines if the given request may be written to a log
//
// Some requests cannot be logged as they may contain user credentials
//
func (c *Client) RequestIsLoggable(req *http.Request) bool {

	if strings.Contains(req.URL.Path, "Authentication") {
		return false
	}

	return true
}
