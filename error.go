package tms

import (
	"fmt"
	"strings"
)

// ValidationErrorResponse is the response structure returned
// When a validation error occurs
type ValidationErrorResponse struct {
	Errors []ValidationError `xml:"ValidationError"`
}

// ValidationError represents a single validation error.
type ValidationError struct {
	AttemptedValue *string `xml:"AttemptedValue"`
	ErrorMessage   string  `xml:"ErrorMessage"`
	Field          *string `xml:"PropertyName"`
}

func (e ValidationErrorResponse) Error() string {
	m := make([]string, len(e.Errors))

	for i, v := range e.Errors {
		m[i] = v.Error()
	}

	return strings.Join(m, "\n")
}

func (e ValidationError) Error() string {
	b := strings.Builder{}

	b.WriteString("[Validation Error.")

	if e.Field != nil {
		b.WriteString("Field = \"")
		b.WriteString(*e.Field)
		b.WriteString("\", ")
	}

	if e.AttemptedValue != nil {
		b.WriteString("Value=\"")
		b.WriteString(fmt.Sprintf("%+v", e.AttemptedValue))
		b.WriteString("\", ")
	}

	b.WriteString("Message=\"")
	b.WriteString(e.ErrorMessage)
	b.WriteString("\"]")

	return b.String()
}
