package tms

import (
	"context"
	"encoding/xml"
	"net/http"
)

// Airline represents known airline details
type Airline struct {
	TMSCode    string  `xml:"AirlineCode"`
	IATACode   *string `xml:"Code"`
	Name       string  `xml:"Name"`
	Descrition string  `xml:"Description"`
	Flags      uint64  `xml:"Flags"`
}

type apiAirlinesResponse struct {
	XMLName  xml.Name  `xml:"ApiAirlinesResponse"`
	Airlines []Airline `xml:"Airlines>Airline"`
}

// GetAirlines gets a list of all airlines
func (c *Client) GetAirlines(ctx context.Context) ([]Airline, error) {
	res := &apiAirlinesResponse{}
	ru := c.BaseURL
	ru.Path += "/Reference/Airlines"

	r, _ := http.NewRequest(http.MethodGet, ru.String(), nil)

	err := c.RequestXML(ctx, r, res)

	return res.Airlines, err
}
